"use strict";

function updatePrice() {

    let s = document.getElementsByName("menu");
    let col = document.querySelector('.colvo');
    let select = s[0];
    let price = 0;
    let prices = getPrices();
    let priceIndex = parseInt(select.value) - 1;
    if (priceIndex >= 0) {
      price = prices.prodTypes[priceIndex] * col.value;

    }
    let radioDiv = document.querySelector(".material");
    radioDiv.style.display = (select.value == "3" || select.value == "2" ? "block" : "none");
    let radio = document.getElementsByName("m");
    radio.forEach(function(radio) {
      if (radio.checked) {
        let optionPrice = prices.prodOptions[radio.value];
        if (optionPrice !== undefined) {
          price += optionPrice;
        }
      }
    });

    let checkDiv = document.querySelector(".checkbox");
    let checkBox = document.getElementById("dc")
    checkDiv.style.display = (select.value == "3" ? "block" : "none" );


      if (checkBox.checked) {
          let propPrice = 10000;
        if (propPrice !== undefined) {
          price += propPrice;
        }
    };

    let btn = document.getElementById("button");
    btn.addEventListener("click", (event) =>{
        event.preventDefault();
        let result = document.getElementById('result');
        result.innerHTML = price + " рублей";

    })
  
  function getPrices() {
    return {
      prodTypes: [4000, 2000, 1000],
      prodOptions: {
        "1": 1000,
        "2": 0
      }
    };
  }
}
  
  window.addEventListener('DOMContentLoaded', function (event) {
    let radioDiv = document.querySelector('.material');
    radioDiv.style.display = "none";
    let checkbox = document.querySelector('.checkbox');
    radioDiv.style.display = "none";
    let s = document.getElementsByName("menu");
    let select = s[0];
    select.addEventListener("change", function(event) {
        console.log(event);
        let checkDiv = document.getElementById('dc');
        let target = event.target;
        let radio = document.getElementsByName("m");
        checkDiv.checked = false;
        radio.forEach(function(radio) {
            if (radio.checked) {
                radio.checked = false;
            }
        })
        let result = document.getElementById('result');
        result.innerHTML = "0" + " рублей";
        updatePrice();
    });

    let col = document.querySelector(".colvo");
    col.addEventListener("blur", (event) => {
        console.log(event);
        let target = event.target;
        updatePrice();
    })
    
    let radios = document.getElementsByName("m");
    radios.forEach(function(radio) {
      radio.addEventListener("change", function(event) {
        console.log(event);
        let r = event.target;
        console.log(r.value);
        updatePrice();
      });
    });
  
    let checkboxes = document.querySelectorAll("#dc")
    checkboxes.forEach(function(checkbox) {
      checkbox.addEventListener("change", function(event) {
        console.log(event);
        let c = event.target;
        console.log(c.name);
        console.log(c.value);
        updatePrice();
      });
    });
  
    updatePrice();
  });
